package workshop.testframework.framework.annotation;

// https://www.mkyong.com/java/java-custom-annotations-example/

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) // Use in method only.
public @interface Test {
	
	//should ignore this test?
	public boolean enabled() default true;
	
}
