package workshop.advancedclassdesign.bad;

public class WeeklyService {
	
	public void run( String taskId){
		
		System.out.println("Task with id: " + taskId  + " is running");
		
	}

}
