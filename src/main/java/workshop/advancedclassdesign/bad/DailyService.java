package workshop.advancedclassdesign.bad;

public class DailyService {
	
	public void run( String taskId){
		
		System.out.println("Task with id: " + taskId  + " is running");
		
	}

}
