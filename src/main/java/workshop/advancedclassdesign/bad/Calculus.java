package workshop.advancedclassdesign.bad;

public class Calculus {
	
	public double executeSomeObscureMathTask( double radius){
		
		return  2D * 3.14D * radius; 

	}

	public double executeSomeOtherObscureMathTask( double radius){
		
		return  3.14D * radius * radius; 
		
	}

}
