package workshop.advancedclassdesign.bad;

/*









 */

public class BadDesign {

	private static float average( int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13){
		
		return (float)(i0 + i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9 + i10 + i11 + i12)/ (float)i13;
		
	}
	
	public static void main(String[] args) {
		
		DailyService dailyService = new DailyService();
		WeeklyService weeklyService = new WeeklyService();
		MonthlyService monthlyService = new MonthlyService();
		
		dailyService.run("daily 1");
		weeklyService.run("weekly 1");
		monthlyService.run("monthly 1");
		
		int[] waardes = {2,4,5,6,3,4,5,1,2,7,3,6,2};
		float average = average(waardes[0], waardes[1], waardes[2], waardes[3], waardes[4], waardes[5], waardes[6], waardes[7], waardes[8], waardes[9], waardes[10], waardes[11], waardes[12], 13);
		System.out.println( "Average: " + average);
		
		double temperatuurWeergegevenInFahrenheit1 = 36;
		double temperatuurWeergegevenInCelcius1 = (temperatuurWeergegevenInFahrenheit1 - 32)*(5D/9D);
		
		double temperatuurWeergegevenInFahrenheit2 = 92;
		double temperatuurWeergegevenInCelcius2 = (temperatuurWeergegevenInFahrenheit2 - 32)*(5D/9D);
		
		double temperatuurWeergegevenInFahrenheit3 = 136;
		double temperatuurWeergegevenInCelcius3 = (temperatuurWeergegevenInFahrenheit3 - 32)*(5D/9D);
		
		double terugGerekendeTemperatuurNaarFahrenheit1 = (temperatuurWeergegevenInCelcius1 * ( 9D/5D)) + 32;
		double terugGerekendeTemperatuurNaarFahrenheit2 = (temperatuurWeergegevenInCelcius2 * ( 9D/5D)) + 32;
		double terugGerekendeTemperatuurNaarFahrenheit3 = (temperatuurWeergegevenInCelcius3 * ( 9D/5D)) + 32;
		
		System.out.println("" + terugGerekendeTemperatuurNaarFahrenheit1);
		System.out.println("" + terugGerekendeTemperatuurNaarFahrenheit2);
		System.out.println("" + terugGerekendeTemperatuurNaarFahrenheit3);
		
		
		Calculus calculus = new Calculus();
		double returnValue1 = calculus.executeSomeObscureMathTask(5.7D);
		double returnValue2 = calculus.executeSomeOtherObscureMathTask(5.7D);
		
		
		System.out.println( "Return value 1:" + returnValue1);
		System.out.println( "Return value 2:" + returnValue2);
		
		
	}

}
