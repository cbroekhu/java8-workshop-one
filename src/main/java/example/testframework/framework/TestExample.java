package example.testframework.framework;

import example.testframework.framework.annotation.Before;
import example.testframework.framework.annotation.Test;
import example.testframework.framework.annotation.TesterInfo;

@TesterInfo(
	priority = TesterInfo.Priority.HIGH,
	createdBy = "mkyong.com",  
	tags = {"sales","test" }
)

public class TestExample {
	
	String query;

	@Test
	void testA() {
	  if (true)
		throw new RuntimeException("This test always failed");
	}

	@Test
	void testB() {
	  if (true)
		throw new RuntimeException("This test always failed");
	}

	@Test(enabled = false)
	void testC() {
	  if (false)
		throw new RuntimeException("This test always passed");
	}

	@Test(enabled = true)
	void testD() {
		System.out.println("Testing query: " + this.query);
	}

	@Before
	void init(){
		System.out.println("Initializing.... ");
		this.query = "SELECT * FROM DUAL";
	}
	
}
