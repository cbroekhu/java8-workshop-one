package example.designpattern.builder;

import java.util.ArrayList;
import java.util.List;

public class ComputerSpec {
	
	private String computerSpec;
	
	private ComputerSpec( String computerSpec){
		
		this.computerSpec = computerSpec;
		
	}
	
	public String getComputerSpec(){
		
		return computerSpec;
		
	}
	
    public static class ComputerSpecBuilder {
    	
    	private String os;   
    	private String processor;
    	private int ram;
    	private String color;
    	private List<String> drives = new ArrayList<>();
    	
    	public ComputerSpecBuilder specifyOs( String os){
    		this.os = os;
    		return this;
    	}
    	
    	public ComputerSpecBuilder insertProcessor( String processor){
    		this.processor = processor;
    		return this;
    	}

    	public ComputerSpecBuilder addMemory( int memory){
    		this.ram = memory;
    		return this;
    	}

    	public ComputerSpecBuilder paint( String color){
    		this.color = color;
    		return this;
    	}

    	public ComputerSpecBuilder mountDrive( String drive){
    		this.drives.add(drive);
    		return this;
    	}
    	
    	public ComputerSpec build(){
    		
    		StringBuilder computerSpec = new StringBuilder(); 

    		computerSpec.append("OS:        " + os + "\n");
    		computerSpec.append("Processor: " + processor + "\n");
    		computerSpec.append("Geheugen:  " + ram + " Gb\n");
    		computerSpec.append("Kleur:     " + color + "\n");
    		computerSpec.append("Schijven:  " + dispDrives() + "\n");
    		
    		return new ComputerSpec( computerSpec.toString());
    		
    	}

		private String dispDrives() {
			
			StringBuilder driveString = new StringBuilder();
			
			for( String drive: drives){
				driveString.append( ", " + drive);
			}
			
			return driveString.toString().substring(2);
			
		}
    	
    }

}
