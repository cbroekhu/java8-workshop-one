package example.designpattern.builder;

import example.designpattern.builder.ComputerSpec.ComputerSpecBuilder;

public class Starter {

	public static void main(String[] args) {
		
		ComputerSpecBuilder computerSpecBuilder = new ComputerSpec.ComputerSpecBuilder();
		
		ComputerSpec computerSpec = computerSpecBuilder
		.specifyOs("Ubuntu 18")
		.insertProcessor("x64")
		.addMemory(16)
		.paint("Black")
		.mountDrive("C: 256 GB")
		.mountDrive("D: 1024 GB")
		.build();
		
		System.out.println( computerSpec.getComputerSpec());
		
	}

}
