package example.designpattern.factory;

import java.io.Closeable;
import java.util.List;

public interface Connection extends Closeable{

    List<String> executeQuery( String query);

}
