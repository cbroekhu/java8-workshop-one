package example.designpattern.factory;

public class DataSourceFactory {
	
	public static final String ORACLE = "ORACLE";
	public static final String SQLSRV = "SQLSRV";
	public static final String MYSQL = "MYSQL";
	public static final String NOSQL = "NOSQL";
	
	private static final DataSourceFactory dataSourcefactory = new DataSourceFactory();
	
	private DataSourceFactory(){
	}

	public static DataSourceFactory getDataSourcefactory(){
		return DataSourceFactory.dataSourcefactory;
	}
	
	public DataSource getDataSource( String type){
		
		DataSource dataSource = null;
		
		switch( type){
			case(ORACLE): dataSource = new OracleDataSource();
			break;
			case(SQLSRV): dataSource = new SqlSrvDataSource();
			break;
			case(MYSQL): dataSource = new MySqlDataSource();
			break;
			case(NOSQL): dataSource = new NoSqlDataSource();
			break;
			default: throw new IllegalArgumentException( "Unkown type: " + type);
		}
		
		return dataSource;
		
	}
	
}
