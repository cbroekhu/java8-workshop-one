package example.designpattern.factory;

import java.io.Closeable;
import java.io.IOException;

public interface DataSource{
	
	public void init(String server, String poort, String db, String user,
			String password)
			throws InstantiationException, IllegalAccessException;

	public Connection getConnection() throws IOException;

}

