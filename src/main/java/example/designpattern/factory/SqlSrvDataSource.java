package example.designpattern.factory;

import java.io.IOException;

public class SqlSrvDataSource implements DataSource{

	private Connection connection;
	
	@Override
	public void init(String server, String poort,
			String db, String user, String password) throws InstantiationException, IllegalAccessException {
		
		// Connect to database
		this.connection = new JdbcConnection( server, poort, db, user, password);
		
		return;
		
	}

	@Override
	public Connection getConnection() throws IOException{
		return this.connection;
	}

}
