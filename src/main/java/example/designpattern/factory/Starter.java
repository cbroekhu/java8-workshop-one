package example.designpattern.factory;

import java.io.IOException;
import java.util.List;

public class Starter {

	DataSource dataSource;

	private void start() {

		try {
			
			dataSource = DataSourceFactory.getDataSourcefactory().getDataSource(DataSourceFactory.ORACLE);
			dataSource.init("server", "1234", "database", "user", "password");
			System.out.println("DataSource: " + dataSource.getClass());
			queryDb(dataSource);
			
			dataSource = DataSourceFactory.getDataSourcefactory().getDataSource(DataSourceFactory.NOSQL);
			dataSource.init("server", "1234", "database", "user", "password");
			System.out.println("DataSource: " + dataSource.getClass());
			queryDb(dataSource);

			dataSource = DataSourceFactory.getDataSourcefactory().getDataSource(DataSourceFactory.MYSQL);
			dataSource.init("server", "1234", "database", "user", "password");
			System.out.println("DataSource: " + dataSource.getClass());
			queryDb(dataSource);

			dataSource = DataSourceFactory.getDataSourcefactory().getDataSource(DataSourceFactory.SQLSRV);
			dataSource.init("server", "1234", "database", "user", "password");
			System.out.println("DataSource: " + dataSource.getClass());
			queryDb(dataSource);
			
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	
	}
	
	private void queryDb(DataSource dataSource) {

		try( Connection connection = dataSource.getConnection()) {

			List<String> results = connection
					.executeQuery("Select * from sometable");

			for (String result : results) {
				System.out.println(result);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		new Starter().start();

	}

}
