package example.designpattern.factory;

import java.util.ArrayList;
import java.util.List;

public class JdbcConnection implements Connection{

    public JdbcConnection(){
    	
    }
    
	public JdbcConnection(String server, String poort, String db, String user,
			String password) {
	}

	@Override
    public List<String> executeQuery(String query) {
        List<String> result = new ArrayList<>();

        result.add("Result 1 user Voldemort");
        result.add("Result 2 user Harry");
        result.add("Result 3 user Perkamentus");

        return result;

    }

    public void close()  {

    }

}
