package example.designpattern.singleton;

public class ThreadSafeSingleton {

    private static ThreadSafeSingleton instance;
    
    private ThreadSafeSingleton(){}
    
    public static synchronized ThreadSafeSingleton getInstance(){
        if(instance == null){
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }
    
    public void connectHttp( String url) throws Exception{
    	
    	
    	// Connect to url
    	
    	
    	
    }
    
    
    
    
}