package example.designpattern.singleton;

public class Starter {

	public static void main(String[] args) {
		
		ThreadSafeSingleton threadSafeSingleton = ThreadSafeSingleton.getInstance();
		
		try {
			threadSafeSingleton.connectHttp("http://www.google.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
