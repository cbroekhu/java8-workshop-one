package example.advancedclassdesign.good;

public class WeeklyService implements Service {

	@Override
	public void run( String taskId){
		
		System.out.println("Task with id: " + taskId  + " is running");
		
	}

}
