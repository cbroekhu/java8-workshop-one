package example.advancedclassdesign.good;

public class Util {
	
	public static double celciusToFahrenheit( double celcius){
		
		return (celcius - 32)*(5D/9D);
		
	}
	
	public static double fahrenheitToCelcius( double fahrenheit){
		
		return ( fahrenheit * ( 9D/5D)) + 32;
		
	}

}
