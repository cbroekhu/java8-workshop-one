package example.advancedclassdesign.good;

public interface Service {

    public void run( String taskId);

}
