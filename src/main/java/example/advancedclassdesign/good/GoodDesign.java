package example.advancedclassdesign.good;

public class GoodDesign {

    public static void main(String[] args) {
    	
		Service dailyService = new DailyService();
		Service weeklyService = new WeeklyService();
		Service monthlyService = new MonthlyService();
		
		dailyService.run("daily 1");
		weeklyService.run("weekly 1");
		monthlyService.run("monthly 1");

        int[] waardes = {2,4,5,6,3,4,5,1,2,7,3,6,2};
        float average = average(waardes);
        System.out.println( "Average: " + average);

		double fahrenheit11 = 36;
		double celsius11 = Util.fahrenheitToCelcius( fahrenheit11);
		
		double fahrenheit12 = 92;
		double celsius12 = Util.fahrenheitToCelcius( fahrenheit12);
		
		double fahrenheit13 = 136;
		double celsius13 = Util.fahrenheitToCelcius( fahrenheit13);
		
		double fahrenheit21 = Util.celciusToFahrenheit(celsius11);
		double fahrenheit22 = Util.celciusToFahrenheit(celsius12);
		double fahrenheit23 = Util.celciusToFahrenheit(celsius13);

		
		System.out.println("" + fahrenheit21);
		System.out.println("" + fahrenheit22);
		System.out.println("" + fahrenheit23);
		
		Calculus calculus = new Calculus();
		double circomference = calculus.calculateCircomference(5.7D);
		double area = calculus.calculateArea(5.7D);
		
		System.out.println( "Circomference: " + circomference);
		System.out.println( "Area: " + area);

    }

    private static float average(int[] waardes) {

        float sum = 0;
        
        for( int i: waardes){
            sum = sum + (float)i;
        }

        return( sum/waardes.length);

    }

}
